﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BobUpDown : MonoBehaviour
{
    Vector3 position;
    public float bobdistance;
    // Start is called before the first frame update
    void Start()
    {
        position = transform.localPosition;
    }

    // Update is called once per frame
    void Update()
    {
        transform.localPosition = position + Vector3.up * (Mathf.Sin(Time.timeSinceLevelLoad) * bobdistance - bobdistance/2) ;
    }
}
