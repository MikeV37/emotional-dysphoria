
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameController : MonoBehaviour {
    static GameController singleton;
    public CameraController cc;
    public HealthDisplay hd;
    public ChargeDisplay dd;
    public Text dayText;
    public Player depressed, manic, standard;
    public Transform levelStart;
    public Player player;
    public int day = 0;
    public FadeInOut fadeToBlack, dayTextFad;
    public RadialFillLerper manCir, depCir, stdCir;

    public int joy, sad;

    public int mania = 1, depression = 1, normal = 1;

    public static void AddJoy(int i) { singleton.joy += i; }

    public static void AddSad(int i) { singleton.sad += i; }
    // Start is called before the first frame update

    void Start() {
        singleton = this;
        newDay();
    }
    private static bool locked;
    public static void endDay() {
        if (!locked) {
            locked = true;
            singleton.newDay();
        }
    }

    public void newDay() {
        //Fade To black
        fadeToBlack.alpha = 1;
        day++;
        if (day > 1) {
            if (joy > sad + 5) { normal++; } else if (sad > joy + 10) { mania++; } else if (sad > joy + 5) { depression++; } else {
                mania++;
                depression += 2;
            }
            mania += Random.Range(0, 2);
            depression += Random.Range(0, 2);
            normal += Random.Range(0, 2);
            mania = Mathf.Max(1, mania - 1);
            depression = Mathf.Max(1, depression - 1);
            normal = Mathf.Max(1, normal - 1);
        }
        joy = 0;
        sad = 0;
        Player p;
        int sum = mania + depression + normal;
        int num = Random.Range(0, sum);
        if (num < mania)
            p = manic;
        else if (num < mania + depression)
            p = depressed;
        else p = standard;
        StartCoroutine(finishFadeOut(p));
        //
    }

    IEnumerator finishFadeOut(Player next) {
        yield return new WaitForSeconds(2f);
        //destroy player character
        Destroy(player.gameObject);
        //Create new player character
        GameObject go = GameObject.Instantiate(next.gameObject, levelStart.position, Quaternion.identity);
        player = go.GetComponent<Player>();
        //Set Up connections
        cc.follow = player.follow;
        cc.onedirection = false;
        hd.p = player;
        dd.p = player;

        dayText.text = "DAY " + day;
        dayText.color = Color.white;
        dayTextFad.HardSet(1, 0);


        float total = mania + depression + normal;
        int ran = -0;// Random.Range(-180, 45);

        manCir.GetComponent<LerpRotate>().desired = Quaternion.Euler(0, 0, ran + (depression + normal) / total * 360);
        depCir.GetComponent<LerpRotate>().desired = Quaternion.Euler(0, 0, ran + (normal) / total * 360);
        stdCir.GetComponent<LerpRotate>().desired = Quaternion.Euler(0, 0, ran);
        manCir.setDesire(mania / total);
        depCir.setDesire(depression / total);
        stdCir.setDesire(normal / total);

        Scene level = SceneManager.GetSceneByName("level");
        if (level.IsValid())
            SceneManager.UnloadSceneAsync(level);
        SceneManager.LoadScene("level", LoadSceneMode.Additive);
        StartCoroutine(finishUpdate());
    }

    IEnumerator finishUpdate() {
        yield return new WaitForSeconds(1f);
        fadeToBlack.alpha = 0;
        cc.onedirection = true;
        StartCoroutine(finishFadeIn());
    }


    IEnumerator finishFadeIn() {
        yield return new WaitForSeconds(1f);
        locked = false;
    }

    // Update is called once per frame
    void Update() {

    }
}
