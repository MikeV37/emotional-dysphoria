using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

public class VolumeIntensityController : MonoBehaviour
{
    Volume posprocess;
    public float intendedWeight;
    // Start is called before the first frame update
    void Start()
    {
        posprocess = GetComponent<Volume>();
    }

    // Update is called once per frame
    void Update()
    {
        posprocess.weight = Mathf.Lerp(posprocess.weight, intendedWeight, Time.deltaTime);
    }
    
    public void AddWeight(float weight) {
        posprocess.weight += weight;
    }
}
