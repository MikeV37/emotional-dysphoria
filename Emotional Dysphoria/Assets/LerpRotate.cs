using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LerpRotate : MonoBehaviour
{
    public Quaternion desired;
    private void Start() {
        desired = transform.rotation;
    }
    // Update is called once per frame
    void Update()
    {
        transform.rotation = Quaternion.Slerp(transform.rotation, desired, Time.deltaTime * 10);
    }
}
