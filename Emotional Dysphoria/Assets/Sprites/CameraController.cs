﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(Camera))]
public class CameraController : MonoBehaviour
{
    public Transform follow;
    public bool onedirection;
    public Vector2 lowerleftbound;
    // Start is called before the first frame update
    void Start()
    {
        lowerleftbound = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        float x = transform.position.x;
        float y = transform.position.y;
        if (!onedirection || transform.position.x < follow.position.x)
            x = Mathf.Lerp(transform.position.x, follow.position.x, Time.deltaTime * 10);
        if (x < lowerleftbound.x)
            x = lowerleftbound.x;
        y = Mathf.Lerp(transform.position.y, follow.position.y, Time.deltaTime * 10);
        if (y < lowerleftbound.y)
            y = lowerleftbound.y;
        transform.position = Vector3.right * x + Vector3.up * y + Vector3.forward * transform.position.z;
    }
}
