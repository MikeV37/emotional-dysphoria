using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RadialFillLerper : MonoBehaviour
{
    Image circle;
    float desire;
    // Start is called before the first frame update
    void Start()
    {
        circle = GetComponent<Image>();
    }

    // Update is called once per frame
    void Update()
    {
        circle.fillAmount = Mathf.Lerp(circle.fillAmount, desire, Time.deltaTime * 10);
    }

    public void setDesire(float set) {
        desire = set;
    }
}
