﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallingPlatform : MonoBehaviour
{
    public SpriteRenderer sr;
    public bool timerstarted;
    public float timer;
    public float timeBeforeFall;
    public Rigidbody2D body;

    private void Start()
    {
        sr = GetComponentInChildren<SpriteRenderer>();
        body = GetComponentInChildren<Rigidbody2D>();
    }

    public void Update()
    {
        if (timerstarted)
        {
            if (timer <= 0)
            {
                body.bodyType = RigidbodyType2D.Dynamic;
                body.gravityScale = 1;
                sr.color = Color.white;
            }
            else
            {
                timer -= Time.deltaTime;
                Color c = sr.color;
                c.r = Mathf.Sin(Time.realtimeSinceStartup % 1);
                c.b = Mathf.Sin(Time.realtimeSinceStartup % 1);
                c.g = Mathf.Sin(Time.realtimeSinceStartup % 1);
                sr.color = c;
            }
        }
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        if (!timerstarted && collision.gameObject.GetComponentInChildren<Player>())
        {
            Player p = collision.gameObject.GetComponentInChildren<Player>();
            if (p.touching)
            {
                timerstarted = true;
                timer = timeBeforeFall;
            }
        }
    }

}
