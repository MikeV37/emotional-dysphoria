using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChargeDisplay : MonoBehaviour
{
    public Player p;
    public Transform powerbar;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(p)
         powerbar.localScale = new Vector3(p.sa.Charge(), 1, 1);
    }
}
