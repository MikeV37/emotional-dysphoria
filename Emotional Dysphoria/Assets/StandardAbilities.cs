﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StandardAbilities : SpecialAbilities
{
    public Player player;
    public SingleUnityLayer enemies;
    public GameObject punch;
    public bool punchReady = true;
    public bool shootReady = true;
    public Vector2 punchForce;

    public int combo = 3, comboMeter = 3;
    public override void Punch()
    {
        if (punchReady)
        {
            player.PlayPunchAnimation();

            StartCoroutine(PunchStop(.1f));
            punchReady = false;
        }
    }
    private Vector2 cancel;

    private IEnumerator PunchStop(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        GameObject go = GameObject.Instantiate(punch, player.transform);
        go.transform.right = player.transform.localToWorldMatrix * player.facing;
        Rigidbody2D rb = player.GetComponentInChildren<Rigidbody2D>();
        rb.AddForce(punchForce * player.facing.x, ForceMode2D.Impulse);
        if (combo > 0)
        {
            combo--;
            StartCoroutine(PunchReady(.1f));
        }
        else {
            combo = comboMeter;
            StartCoroutine(PunchReady(.5f));
        }
        
    }

    private IEnumerator PunchReady(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        punchReady = true;
    }
    public GameObject shot;

    private IEnumerator ShootReady(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        shootReady = true;
    }

    private IEnumerator ShootStop(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        StartCoroutine(ShootReady(.5f));
        GameObject go = GameObject.Instantiate(shot, player.transform.position, player.transform.rotation);
        go.GetComponent<Impulse>().force *= player.facing.x;
        //go.transform.right = player.transform.localToWorldMatrix * player.facing;
        


    }
    public override void Shoot()
    {
        if (shootReady)
        {
            player.PlayShootAnimation();
            StartCoroutine(ShootStop(.5f));
            shootReady = false;
        }
    }

    public int chargeup;
    public int chargeupReady;

    public bool specialReady;

    public GameObject special;
    public override void Special()
    {
        if (specialReady && chargeup >= chargeupReady && special)
        {
            chargeup = 0;
            player.PlaySpecialAnimation();

            StartCoroutine(SpecialStop(.3f));
            specialReady = false;
        }
    }
    private IEnumerator SpecialStop(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        GameObject go = GameObject.Instantiate(special, player.transform.position, player.transform.rotation);
        go.transform.right = player.transform.localToWorldMatrix * player.facing;
        StartCoroutine(SpecialReady(.5f));
    }

    private IEnumerator SpecialReady(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        specialReady = true;
    }

    public override float Charge() {
        if (special)
            return 1f * chargeup / chargeupReady;
        else return 0;
    }
}
