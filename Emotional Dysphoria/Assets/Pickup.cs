using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Pickup : MonoBehaviour {
	public int sad;
	public GameObject effect;
	public int joy;
	public Color color;
	public AudioClip noise;
	public List<string> text;
	private void OnTriggerEnter2D(Collider2D collision) {
		if(collision.gameObject.GetComponent<Player>()) {
			GameController.AddSad(sad);
			GameController.AddJoy(joy);
			if (text.Count > 0)
				AnxietyText.WriteMessage(text[Random.Range(0,text.Count)],color,noise);
			Destroy(gameObject);
		}
	}

}
