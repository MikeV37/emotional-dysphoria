using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Anxie :  Damageable
{
    public int health = 3;
    public float scaleFactor = 1;
    public GameObject poof;
    public LayerMask platformLayerMask;
    public Transform body;
    public Rigidbody2D rb;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        scaleFactor = transform.localScale.x;
    }
    public Vector2 force;
    // Update is called once per frame
    public virtual void Update()
    {
        rb.AddForce(force);
        CircleCollider2D boxCollider = GetComponent<CircleCollider2D>();
        RaycastHit2D hit = Physics2D.BoxCast(boxCollider.bounds.center, boxCollider.bounds.size/2, 0, force + Vector2.down * force.magnitude, 3, platformLayerMask);
        Debug.DrawRay(boxCollider.bounds.center, (force + Vector2.down * force.magnitude).normalized * 3);
        if (hit.collider != null) {
           
        } else {
            force *= -1;
            setAngle();
        }

        RaycastHit2D hitWall = Physics2D.BoxCast(boxCollider.bounds.center, boxCollider.bounds.size / 2, 0, force, 2, platformLayerMask);
        Debug.DrawRay(boxCollider.bounds.center, (force).normalized * 2);
        if (hitWall.collider != null) {
            force *= -1;
            setAngle();
        } else {
            
        }
        
    }

   public override void Damage(int i) {
        health -= i;
        if (health <= 0) {
            GameObject.Instantiate(poof, transform.position, Quaternion.identity);
            Destroy(gameObject);
        }
    }

    public void setAngle() {
        if(force.x > 0) {
            transform.localScale = new Vector3(-1, 1, 1) * scaleFactor;
            //body.rotation = Quaternion.Euler(0, 180, 0);
        }
        else {
            transform.localScale = Vector3.one * scaleFactor;
            //body.rotation = Quaternion.Euler(0, 0, 0);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision) {
        /*if (collision.gameObject.GetComponent<Damager>()) {
            Damage(collision.gameObject.GetComponent<Damager>().damage);
        }*/
    }

    protected virtual void OnTriggerEnter2D(Collider2D collision) {
        if (collision.GetComponent<Player>()) {
            GetComponent<Animator>().ResetTrigger("Bite");
            GetComponent<Animator>().SetTrigger("Bite");
            collision.GetComponent<Player>().Damage(1);
        }
    }
}


public abstract class Damageable : MonoBehaviour {
    abstract public void Damage(int i);
}