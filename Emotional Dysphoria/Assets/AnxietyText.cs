using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AnxietyText : MonoBehaviour
{
    static AnxietyText singleton;
    public GameObject oofmessage;
    public void Start() {
        singleton = this;
    }

    public static void WriteMessage(string s, Color c, AudioClip a) {
        Text t = GameObject.Instantiate(singleton.oofmessage, singleton.transform).GetComponent<Text>();
        t.text = s;
        t.color = c;
        if(a)
            t.GetComponent<PlayRandomClip>().clips = new List<AudioClip>(new AudioClip[]{a});
    }

    public static void WriteMessage(string s, Color c) {
        WriteMessage(s, c, null);
    }
    public static void WriteMessage(string s) {
        WriteMessage(s, Color.black);
    }
    public static List<string> messages = new List<string>(new string[]{
        "You're worthless",//M
        "Nobody loves you",//S
        "Why Even bother",//Y
        "She never cared",//M
        "You're ugly",//S
        "you can't do anything",//Y
        "what a piece of shit you are",//M
        "You'll always be alone",//M
        "Fuck you",//S
        "There's no hope",//Y
        "Just end it all",//M
        "Stop trying",//S
        "Kill yourself",//Y
        "You'll never measure up",//S
        "Nothing you do matters",//Y
        "You're going to die alone",//Y
        "Everyone fits in but you",//Y
        "You don't know what you're doing",//Y
        "They all know how fucking weird you are",
        "The way you walk is weird, you look like an alien",
        "Your ex was right anout you",
        "Mom doesn't think you can succeed",
        "Your phone is always silent",
        "She left you on read"


    });
    public static void WriteMessage() {
        WriteMessage(messages[Random.Range(0, messages.Count)]);
    }
}
