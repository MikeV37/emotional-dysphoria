﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DepressedAbilities : SpecialAbilities
{
    public Player player;
    public SingleUnityLayer transparent;
    public SingleUnityLayer solid;
    public bool dodgeReady = true;
    public bool shootReady = true;
    
    public override void Punch()
    {
        if (dodgeReady)
        {
            player.PlayShootAnimation();
            Rigidbody2D rb = player.GetComponentInChildren<Rigidbody2D>();
            Vector2 push = player.facing * 15;
            rb.AddForce(push, ForceMode2D.Impulse);
            rb.AddTorque(player.facing.x * -100);
            cancel = push * -.2f;
            dodgeReady = false;
            StartCoroutine(DodgeStop(.5f));
            player.gameObject.layer = transparent.LayerIndex;
            foreach (SpriteRenderer sr in player.GetComponentsInChildren<SpriteRenderer>())
            {
                Color clear = Color.white;
                clear.a = .5f;
                sr.color = clear;
            }
        }
    }
    private Vector2 cancel;

    private IEnumerator DodgeStop(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        foreach (SpriteRenderer sr in player.GetComponentsInChildren<SpriteRenderer>())
        {
            sr.color = Color.white;
        }
        player.gameObject.layer = solid.LayerIndex;
        Rigidbody2D rb = player.GetComponentInChildren<Rigidbody2D>();

        rb.velocity /= 2;
        rb.angularVelocity /= 2;
        StartCoroutine(DodgeReady(2));
    }

    private IEnumerator DodgeReady(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        dodgeReady = true;
    }
    public GameObject shot;

    private IEnumerator ShootReady(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        shootReady = true;
    }
    public override void Shoot()
    {
        if (shootReady)
        {
            player.PlayPunchAnimation();
            GameObject go = GameObject.Instantiate(shot, player.transform.position, player.transform.rotation);
            Rigidbody2D rb = player.GetComponentInChildren<Rigidbody2D>();
            go.GetComponent<Impulse>().force = go.GetComponent<Impulse>().force.x * player.facing.x * Vector2.right + go.GetComponent<Impulse>().force.y * Vector2.up + (Vector2)rb.velocity;
        
            StartCoroutine(ShootReady(.2f));
            shootReady = false;
        }
    }
    public bool sinkReady = true;
    private IEnumerator SinkStop(float waitTime)
    {

        yield return new WaitForSeconds(waitTime);
        sinkReady = true;
        foreach (SpriteRenderer sr in player.GetComponentsInChildren<SpriteRenderer>())
        {
            sr.color = Color.white;
        }
        player.gameObject.layer = solid.LayerIndex;
        StartCoroutine(SinkReady(2f));
    }
    private IEnumerator SinkReady(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        sinkReady = true;
    }
    public override void Special()
    {
        if (sinkReady)
        {
            player.PlaySpecialAnimation();
            player.gameObject.layer = transparent.LayerIndex;
            foreach (SpriteRenderer sr in player.GetComponentsInChildren<SpriteRenderer>())
            {
                Color clear = Color.white;
                clear.a = .5f;
                sr.color = clear;
            }
            StartCoroutine(SinkStop(3f));
        }
    }

    public override float Charge() {
        return 0;
    }
}
