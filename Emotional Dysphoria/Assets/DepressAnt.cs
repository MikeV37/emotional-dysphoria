using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DepressAnt : Paranoid
{
    public float speedMin, speedMax;
    public float jumpforce;
    public bool grounded;
    public Vector2 targetPos;

    // Update is called once per frame
    public override void Update() {
        if (target) {
            if (timer > 0)
                timer -= Time.deltaTime;
            targetPos = target.position;
            speed = speedMax;
        } else if (timer < 0) {
            speed = speedMin;
            timer = Random.Range(.5f, 2f);
            targetPos = (Vector2)transform.position + Random.insideUnitCircle * 5;
        } else timer -= Time.deltaTime;

        GetComponent<Animator>().SetFloat("speed", rb.velocity.magnitude);
        GetComponent<Animator>().SetBool("grounded", grounded);
        rb.AddForce(force * speed);
        CircleCollider2D boxCollider = GetComponent<CircleCollider2D>();
        RaycastHit2D hit = Physics2D.BoxCast(boxCollider.bounds.center, boxCollider.bounds.size / 2, 0, force + Vector2.down * force.magnitude, 2, platformLayerMask);
        Debug.DrawRay(boxCollider.bounds.center, (force + Vector2.down * force.magnitude).normalized * 2);
        if (hit.collider != null) {
            grounded = true;
        } else {
            if(grounded && targetPos.y > transform.position.y)
                rb.AddForce(Vector2.up * jumpforce); ;
            //force *= -1;
            setAngle();
            grounded = false;
        }

        if(transform.position.x > targetPos.x) {
            force = Vector2.left;
        } else {
            force = Vector2.right;
        }
        setAngle();
        
        RaycastHit2D hitWall = Physics2D.BoxCast(boxCollider.bounds.center, boxCollider.bounds.size / 2, 0, force, 1, platformLayerMask);
        Debug.DrawRay(boxCollider.bounds.center, (force).normalized * 1);
        if (hitWall.collider != null) {
            float dropfactor = Mathf.Abs(transform.position.x - targetPos.x);
            if (targetPos.y > transform.position.y - dropfactor) 
            {
                if(transform.localScale.x > 0)
                    body.rotation = Quaternion.Slerp(body.rotation, Quaternion.Euler(0, 0, -90),Time.deltaTime * 5);
                else
                    body.rotation = Quaternion.Slerp(body.rotation, Quaternion.Euler(0, 0, 90), Time.deltaTime * 5);
                rb.AddForce(Vector2.up * speed * 5);
            } else {
                body.rotation = Quaternion.Slerp(body.rotation, Quaternion.Euler(0, 0, 0), Time.deltaTime * 5);
            }
        } else {
            body.rotation = Quaternion.Slerp(body.rotation, Quaternion.Euler(0, 0, 0), Time.deltaTime * 5);
        }
    }
}
