using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Damager : MonoBehaviour
{
	public int damage;

	private void OnParticleCollision(GameObject other) {
		if (other.GetComponent<Damageable>()) {
			other.GetComponent<Damageable>().Damage(damage);
		}
	}

	private void OnCollisionEnter2D(Collision2D collider) {
		GameObject other = collider.gameObject;
		if (other.GetComponent<Damageable>()) {
			other.GetComponent<Damageable>().Damage(damage);
		}
	}
}
