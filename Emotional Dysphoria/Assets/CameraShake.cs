﻿using UnityEngine;
using System.Collections;

public class CameraShake : MonoBehaviour {
	// Transform of the camera to shake. Grabs the gameObject's transform
	// if null.
	public Transform camTransform;

	// How long the object should shake for.
	public float shakeDuration = 0f;

	// Amplitude of the shake. A larger value shakes the camera harder.
	public float shakeAmount = 0.7f;
	public float decreaseFactor = 1.0f;

	Vector3 displace;

	void Awake() {
		if (camTransform == null) {
			camTransform = GetComponent(typeof(Transform)) as Transform;
		}
	}

	public void ShakeMore(float shake) {
		shakeDuration += shake;
	}

	public void Shake(float shake) {
		shakeDuration = Mathf.Max(shake, shakeDuration);
	}

	void OnEnable() {
		//originalPos = camTransform.localPosition;
	}

	void Update() {

		if (shakeDuration > 0) {
			Vector2 camPositiobn = camTransform.localPosition - displace;
			displace = (Vector2)(Random.insideUnitSphere * shakeAmount * shakeDuration);
			camTransform.localPosition += displace;
			shakeDuration -= Time.deltaTime * decreaseFactor;
		} else {
			shakeDuration = 0f;
			if (displace.sqrMagnitude > 0) {
				camTransform.localPosition -= displace;
				displace = Vector2.zero;
			}
		}
	}
}