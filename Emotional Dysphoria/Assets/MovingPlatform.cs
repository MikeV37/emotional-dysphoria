﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatform : MonoBehaviour
{
    public Transform platform, start, end;
    bool updown;
    public float speed, deadzone;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(updown)
        {
            platform.position = Vector2.MoveTowards(platform.position, end.position, Time.deltaTime * speed);
            if(Vector2.Distance(platform.position,end.position) < deadzone)
            {
                updown = false;
            }
        }
        else if (!updown)
        {
            platform.position = Vector2.MoveTowards(platform.position, start.position, Time.deltaTime * speed);
            if (Vector2.Distance(platform.position, start.position) < deadzone)
            {
                updown = true;
            }
        }
    }
}
