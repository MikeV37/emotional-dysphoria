using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayRandomClip : MonoBehaviour
{
    public List<AudioClip> clips;
    public AudioSource src;
    // Start is called before the first frame update
    void Start()
    {
        src = GetComponent<AudioSource>();
        src.PlayOneShot(clips[Random.Range(0, clips.Count)]);
    }

 
}
