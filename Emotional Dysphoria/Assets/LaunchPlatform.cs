﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaunchPlatform : MonoBehaviour
{
    public Vector2 push;
    public float distance;
    private void OnTriggerStay2D(Collider2D collision)
    {
        collision.attachedRigidbody.AddForce(transform.localToWorldMatrix * push * Mathf.Max(0,distance - Vector2.Distance(transform.position, collision.gameObject.transform.position)));
    }
}
