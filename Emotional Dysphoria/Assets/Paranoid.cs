using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class Paranoid : Anxie
{

    public Transform target;
    public float speed;

    public float timer;
    // Update is called once per frame
    public override void Update()
    {
        if (target) {
            rb.AddForce((target.position - transform.position).normalized * speed, ForceMode2D.Force);
            if(timer > 0)
                timer -= Time.deltaTime;
        } else if (timer < 0) {
            timer = Random.Range(.5f, 2f);
            rb.AddForce((Random.insideUnitCircle).normalized * speed * 100, ForceMode2D.Force);
        } else timer -= Time.deltaTime;
        force = rb.velocity;
        setAngle();
    }

    protected override void OnTriggerEnter2D(Collider2D collision) {
        if (collision.GetComponent<Player>()) {
            target = collision.transform;
        }
    }



    void OnTriggerStay2D(Collider2D collision) {
        if (collision.GetComponent<Player>()) {
            if (target && (target.position - transform.position).magnitude < 1.75f && timer <=0) {
                timer = 1f;
                base.OnTriggerEnter2D(collision);
            } 
        }
    }

    protected virtual void OnTriggerExit2D(Collider2D collision) {
        if (collision.GetComponent<Player>()) {
            target = null;
        }
    }
}
