using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class FadeInOut : MonoBehaviour {
    Image i;
    Text t;
    public float alpha;
    // Start is called before the first frame update
    public virtual void Start() {
        t = GetComponent<Text>();
        i = GetComponent<Image>();
    }

    // Update is called once per frame
    public virtual void Update() {
        Color c = Color.white;
        if (i)
            c = i.color;
        else if (t)
            c = t.color;
        c.a = Mathf.Lerp(c.a, alpha, Time.deltaTime * 5);
        if (i)
            i.color = c;
        if (t)
            t.color = c;
    }

    public void HardSet(float alpha,float transitionto) {
        Color c = Color.white;
        c.a = alpha;
        if (i)
            i.color = c;
        if (t)
            t.color = c;
        alpha = transitionto;
    }
}
