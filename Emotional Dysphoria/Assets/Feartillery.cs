using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Feartillery : Paranoid
{
    public float recharge;
    public Impulse shot;
    // Update is called once per frame
    public override void Update()
    {
        if (target) {
            if (timer < 0) {
                Impulse fired = GameObject.Instantiate(shot.gameObject, transform.position + transform.up * 1, Quaternion.identity, null).GetComponent<Impulse>();
                float x = (target.position.x - transform.position.x) /2;
                GetComponent<Animator>().ResetTrigger("Shoot");
                GetComponent<Animator>().SetTrigger("Shoot");
                float y = 8 + (target.position.y - transform.position.y) ;// Mathf.Abs(x);
                fired.force = new Vector2(x, y);
                timer = recharge;
            } else timer -= Time.deltaTime;
        } else {
            timer = recharge;
        }
    }

}
