using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hangup : MonoBehaviour
{
    public Animator animator;
    public Rigidbody2D tongue;
    public List<GameObject> attachables;
    public float wait;
    void Start() {
        animator.SetFloat("wait", wait);
    }
    private void OnCollisionEnter2D(Collision2D collision) {
        animator.SetFloat("speed", -1);
        if(collision.rigidbody && collision.rigidbody.bodyType == RigidbodyType2D.Dynamic && collision.rigidbody.GetComponent<SpringJoint2D>() == null) {
            SpringJoint2D fs = collision.gameObject.AddComponent<SpringJoint2D>();
            fs.connectedBody = tongue;
            fs.autoConfigureDistance = false;
            fs.autoConfigureConnectedAnchor = false;
            attachables.Add(collision.gameObject);
        }
    }

    public void Update() {
        if(animator.GetCurrentAnimatorStateInfo(0).normalizedTime <=0) {
            foreach(GameObject go in attachables) {
                if (go.GetComponent<SpringJoint2D>()) {
                    Destroy(go.GetComponent<SpringJoint2D>());
                    if (go.GetComponent<Damageable>()) {
                        go.GetComponent<Damageable>().Damage(1);
                    }
                }
            }
            animator.SetFloat("speed", 1);
            attachables.Clear();
        }
            
    }
}
