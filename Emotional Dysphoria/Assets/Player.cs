﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;
public class Player : Damageable
{

    Rigidbody2D body;
    BoxCollider2D boxCollider;
    public LayerMask platformLayerMask;
    public float jumpstrength = 5;
    public float movestrength = 10;
    public Animator bodyAnimator;
    public SpecialAbilities sa;
    public Vector2 facing = Vector2.right;

    public float airmobility;
    public int setdoubljumps;

    public int health;

    public Transform follow;

    public AudioSource auSrc;
    public AudioClip step, jump;

    VolumeIntensityController damagevic;

    public override void Damage(int j) {
        for(int i = 0; i < j; i++) {
            Damage();
        }
        if (health <= 0) {
            GameController.AddSad(10);
            GameController.endDay();
        }
    }
    private void Damage() {
        if (health <= 0)
            return;
        health--;

        AnxietyText.WriteMessage();
        Camera.main.GetComponent<CameraShake>().Shake(.5f);
        if (!damagevic)
            damagevic = GameObject.FindObjectOfType<VolumeIntensityController>();
        damagevic.AddWeight(.3f);
	}

	private int doublejumps;
    // Start is called before the first frame update
    void Start()
    {
        body = GetComponent<Rigidbody2D>();
        boxCollider = GetComponent<BoxCollider2D>();
    }

    // Update is called once per frame
    void Update()
    {
        bodyAnimator.SetBool("GroundTouching", touching);
        bodyAnimator.SetFloat("VerticalSpeed", body.velocity.y);
        bodyAnimator.SetFloat("HorizontalSpeed", Mathf.Abs(body.velocity.x));
        if(body.velocity.x > 0.1f)
            bodyAnimator.transform.localRotation = Quaternion.Euler(0, 0, 0);
        else if(body.velocity.x < -0.1f)
            bodyAnimator.transform.localRotation = Quaternion.Euler(0, 180, 0);
    }

    private void LateUpdate()
    {
        /*
        bodyAnimator.SetBool("Shoot", false);
        bodyAnimator.SetBool("Special", false);
        bodyAnimator.SetBool("Punch", false);
        */
    }
    float steptimer;
    // Update is called once per frame
    void FixedUpdate()
    {
        RaycastHit2D hit = Physics2D.BoxCast(boxCollider.bounds.center, boxCollider.bounds.size, 0f, Vector2.down, 0.1f, platformLayerMask);
        if (hit.collider != null)
        {
            touching = true;
        }
        else
        {
            touching = false;
        }
        if (touching)
        {
            SelfRight();
            doublejumps = setdoubljumps;
            body.AddForce(Vector2.right * force, ForceMode2D.Force);
            steptimer -= body.velocity.magnitude * Time.deltaTime;
            if (steptimer < 0) {
                steptimer = .5f;
                auSrc.PlayOneShot(step, UnityEngine.Random.Range(.2f,.6f));
            }
        }
        else
        {
            body.AddForce(Vector2.right * force * airmobility, ForceMode2D.Force);
        }


    }
    public float selfrightforce = 15;
    public void SelfRight()
    {
        Vector3 targetDelta = Vector3.up - transform.up;

        //get the angle between transform.forward and target delta
        float angleDiff = Vector3.Angle(transform.up, Vector3.up);

        // get its cross product, which is the axis of rotation to
        // get from one vector to the other
        Vector3 cross = Vector3.Cross(transform.up, targetDelta);

        // apply torque along that axis according to the magnitude of the angle.
        body.AddTorque(cross.z * angleDiff * selfrightforce);
    }

    public float force;
    public bool touching;
    public void OnMove(InputAction.CallbackContext obj)
    {
        force = obj.ReadValue<float>() * movestrength;
        if(force != 0)
            facing = (obj.ReadValue<float>() * Vector2.right).normalized;

    }

    public void OnJump(InputAction.CallbackContext obj)
    {
        if (obj.ReadValueAsButton()) {
            if (touching || doublejumps > 0)
            {
                if (!touching)
                    doublejumps--;
                auSrc.PlayOneShot(jump);
                body.AddForce(Vector2.up * jumpstrength, ForceMode2D.Impulse);
            }
        }
    }
    public void PlayShootAnimation()
    {
        bodyAnimator.SetBool("Shoot", true);
        bodyAnimator.SetBool("Special", false);
        bodyAnimator.SetBool("Punch", false);
        bodyAnimator.SetBool("Unlocked", false);
    }
    public void PlayPunchAnimation()
    {
        bodyAnimator.SetBool("Punch", true);
        bodyAnimator.SetBool("Shoot", false);
        bodyAnimator.SetBool("Special", false);
        bodyAnimator.SetBool("Unlocked", false);
    }
    public void PlaySpecialAnimation()
    {
        bodyAnimator.SetBool("Special", true);
        bodyAnimator.SetBool("Shoot", false);
        bodyAnimator.SetBool("Punch", false);
        bodyAnimator.SetBool("Unlocked", false);
    }

    public void OnPunch(InputAction.CallbackContext obj)
    {
        if (!obj.ReadValueAsButton()) { return; }
        //Trigger punch animation\
        //bodyAnimator.ResetTrigger("Punch");
       

        sa.Punch();
        //punch reload
        //do damage to the enemies
        //pushback?
    }
    public void OnShoot(InputAction.CallbackContext obj)
    {
        if (!obj.ReadValueAsButton()) { return; }
        //Shoot a projectile
        //bodyAnimator.ResetTrigger("Shoot");
       
        //trigger animation
        //

        sa.Shoot();
    }
    public void OnSpecial(InputAction.CallbackContext obj)
    {
        if (!obj.ReadValueAsButton()) { return; }
        //do the special move
        //bodyAnimator.ResetTrigger("Special");
      

        sa.Special();
    }

}


public abstract class SpecialAbilities : MonoBehaviour
{
    public abstract float Charge();
    public abstract void Punch();

    public abstract void Shoot();

    public abstract void Special();
}