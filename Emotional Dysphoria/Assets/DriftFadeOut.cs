﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DriftFadeOut : MonoBehaviour
{

    public Text t;
    public Outline o;
    // Start is called before the first frame update
    void Start()
    {
        t = GetComponent<Text>();
        o = GetComponent<Outline>();
        
        move = Random.insideUnitCircle * 10;
        RectTransform rt = GetComponent<RectTransform>();
        rt.anchoredPosition = Random.insideUnitCircle * Random.Range(0, 200);
    }
    Vector3 move;
    // Update is called once per frame
    void Update()
    {

        transform.position += move * Time.deltaTime;
        Color c = t.color;
        c.a -= Time.deltaTime;
        t.color = c;
        Color k = o.effectColor;
        k.a -= Time.deltaTime;
        o.effectColor = k;
        if (c.a <= 0)
            Destroy(this.gameObject);
    }
}
