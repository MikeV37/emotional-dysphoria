using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadeOutFollower : FadeInOut
{
    FadeInOut parent;
    // Start is called before the first frame update
    public override void Start()
    {
        base.Start();
        parent = transform.parent.GetComponentInParent<FadeInOut>();
    }

    // Update is called once per frame
    public override void Update()
    {
        alpha = parent.alpha;
        base.Update();
    }
}
