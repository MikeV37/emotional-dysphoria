using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spikes : MonoBehaviour
{
    private void OnTriggerStay2D(Collider2D collision) {
        if (collision.gameObject.GetComponent<Player>()) {
            collision.gameObject.GetComponent<Player>().Damage(1);
        }    
    }
}
