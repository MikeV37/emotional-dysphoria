using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class HealthDisplay : MonoBehaviour
{
    public Player p;
    public List<Image> hearts;

    // Update is called once per frame
    void Update()
    {
        if (p) {
            hearts.ForEach(x => x.enabled = false);
            for (int i = 0; i < p.health; i++) {
                hearts[i].enabled = true;
            }
        }
    }
}
