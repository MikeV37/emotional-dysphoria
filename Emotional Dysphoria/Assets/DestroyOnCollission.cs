﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyOnCollission : MonoBehaviour
{
    public GameObject explosion;
    public LayerMask layermask;
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (layermask == (layermask | (1 << collision.gameObject.layer))){
            GameObject.Instantiate(explosion, transform.position, transform.rotation);
            Destroy(gameObject);
        }
    }
}
